package com.ndkj.provider.service;

import com.ndkj.providercommon.client.ProviderService;
import com.ndkj.rpcserver.protocol.HttpServer;
import com.ndkj.rpcserver.register.LocalRegister;

public class ProvideRegistrationStarter {

    /**
     * Step 1
     * 完成服务注册，启动RPC-Server模块下的tomcat并hold住，由RPC-Server完成泛化调用
     */
    public static void main(String[] args) {
        // service provider完成多版本注册
        LocalRegister.register(ProviderService.class.getName(), "1.0.0", ProviderServiceImpl.class);
        LocalRegister.register(ProviderService.class.getName(), "1.0.0.pre", ProviderServiceImpl.class);
        LocalRegister.register(ProviderService.class.getName(), "1.0.0.daily", ProviderServiceImpl.class);

        // 启动tomcat
        HttpServer httpServer = new HttpServer();
        httpServer.start("localhost", 8080);
    }

}
