package com.ndkj.provider.service;

import com.ndkj.providercommon.client.ProviderService;

public class ProviderServiceImpl implements ProviderService {

    @Override
    public String doSome(String str) {
        String prefixMsg = "The call succeeded.\nhello";
        String suffixMsg = "Welcome to http://www.liuyo.top for more information.";
        return String.format("%s %s, %s",prefixMsg, str, suffixMsg);
    }

}
