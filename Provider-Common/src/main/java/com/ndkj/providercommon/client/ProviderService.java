package com.ndkj.providercommon.client;

/**
 * consumer消费的接口定义
 */
public interface ProviderService {

    String doSome(String str);

}
