# RpcDemo

#### 介绍
手写rpc，tomcat本地注册中心+泛化调用，有助于初学者理解rpc调用过程。

#### 软件架构
注册中心：单机Tomcat+Map
协议：http
序列化方式：JDK


#### 使用说明

1. Provider模块运行main方法，注册服务，启动tomcat
2. Consumer模块运行main方法测试

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request