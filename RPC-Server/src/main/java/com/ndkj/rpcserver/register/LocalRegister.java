package com.ndkj.rpcserver.register;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Map本地注册中心
 */
public class LocalRegister {

    private static Map<String, Class> map = new ConcurrentHashMap<>();

    public static void register(String interfaceName, String version, Class implClazz) {
        map.put(String.format("%s%s", interfaceName, version), implClazz);
    }

    public static Class get(String interfaceName, String version) {
        return map.get(String.format("%s%s", interfaceName, version));
    }

}
